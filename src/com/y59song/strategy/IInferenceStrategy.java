package com.y59song.strategy;

import com.y59song.utilities.SigWin;

/**
 * Created by frank on 2/23/2014.
 */
public abstract class IInferenceStrategy extends IStrategy {
  abstract public int[] classify(SigWin[] dataWin, int[] start, int[] end);
  abstract public double[] getProbability(SigWin[] dataWin, int[] start, int[] end);
}
