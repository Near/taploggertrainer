package com.y59song.strategy;

import com.y59song.utilities.SigWin;

/**
 * Created by frank on 2/23/2014.
 */
public abstract class IStrategy {
  public final static double base = 9.8 * 9.8;
  abstract public void importModel(String name);
  abstract public void exportModel(String name);
  abstract public void learn(SigWin data[], int start[], int end[], int res);
  abstract public double[] getFeature(SigWin[] data, int[] start, int[] end);

  public String getCase(double[] features, int res) {
    String s = res == -1 ? "" : "" + res + " ";
    for(int i = 0; i < features.length; i ++) s += "" + (i+1) + ":" + features[i] + " ";
    return s;
  }
}
