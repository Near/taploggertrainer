package com.y59song.strategy;

import com.y59song.utilities.SigWin;

/**
 * Created by frank on 2/23/2014.
 */
public abstract class IDetectionStrategy extends IStrategy {
  abstract public boolean isTap(SigWin[] dataWin, int start[], int end[]);
  abstract public int[] getTap(SigWin[] dataWin, int end[]);
  public double getProbability(SigWin[] dataWin, int start[], int end[]) {
    return 1;
  }
}
