package com.y59song.strategy;

import android.util.Log;
import com.y59song.utilities.SigWin;
import com.y59song.utilities.Utility;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by frank on 2/23/2014.
 */
public class NewTapLoggerDetection extends IDetectionStrategy {
  private ArrayList<Double>[] datas;
  private double[] features;
  private final int feature_num = 11;

  public NewTapLoggerDetection() {
    features = new double[feature_num * 2];
    datas = new ArrayList[feature_num];
    for(int i = 0; i < feature_num; i ++) datas[i] = new ArrayList<Double>();
  }

  @Override
  public boolean isTap(SigWin[] dataWin, int start[], int end[]) {
    double p[] = getFeature(dataWin, start, end);
    if(p == null) return false;
    for(int i = 0; i < p.length; i ++) {
      if(p[i] > features[i*2+1] || p[i] < features[i*2]) return false;
    }
    return true;
  }

  public int[] getTap(SigWin[] dataWin, int end[]) {
    int ret[] = new int[end.length], left = Math.max(end[0] - (int)(features[feature_num * 2 - 1]), 0),
      right = Math.max(end[0] - (int)(features[feature_num * 2 - 2]), 0), mid = (left + right) / 2;
    if(mid == 0) return null;
    boolean valid = false;
    for(int dis = 0; dis <= right - mid; dis ++) {
      if(mid - dis >= left && isTap(dataWin, new int[]{mid - dis, 0, 0}, end)) {
        ret[0] = mid - dis;
        valid = true;
      } else if(mid + dis <= right && isTap(dataWin, new int[]{mid + dis, 0, 0}, end)) {
        ret[0] = mid + dis;
        valid = true;
      }
      if(valid) {
        for(int j = end[1]; j >= 0; j --) {
          if(dataWin[1].getTimeAt(j) <= dataWin[0].getTimeAt(ret[0])) {
            ret[1] = j;
            return ret;
          }
        }
      }
    }
    return null;
  }

  @Override
  public void importModel(String name) {
    File file = new File(name);
    BufferedReader br;
    try {
      br = new BufferedReader(new FileReader(file));
      if(br == null) return;
      for(int i = 0; i < feature_num; i ++) {
        try {
          features[i * 2] = Double.parseDouble(br.readLine());
          features[i * 2 + 1] = Double.parseDouble(br.readLine());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  public void exportModel(String name) {
    File file = new File(name);
    FileWriter fw;
    Log.v(this.getClass().getSimpleName(), "Export file name : " + name);
    try {
      fw = new FileWriter(file);
      if(fw == null) return;
      //file.delete();
      for(int i = 0; i < feature_num; i ++) {
        double data[] = new double[datas[i].size()];
        for(int j = 0; j < data.length; j ++) data[j] = datas[i].get(j);
        double range[] = Utility.getRange(data);
        System.arraycopy(range, 0, features, i * 2, 2);
      }
      for(int i = 0; i < features.length; i ++) fw.write("" + features[i] + "\n");
      fw.flush();
      fw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void learn(SigWin[] data, int[] start, int[] end, int res) {
    double []p = getFeature(data, start, end);
    if(p == null) return;
		for(int i = 0; i < p.length; i ++) datas[i].add(p[i]);
  }

  @Override
  public double[] getFeature(SigWin[] data, int[] start, int[] end) {
    int length = end[1] - start[1] + 1, id[] = new int[4];
    double v[][] = new double[2][length];
    for(int i = 0; i < length; i ++) {
      v[0][i] = data[1].getValueAt(i + start[1], 1);
      v[1][i] = data[1].getValueAt(i + start[1], 2);
    }
    for(int i = 0; i < 2; i ++) {
      id[i * 2] = Utility.getMaxIndex(v[i]);
      id[i * 2 + 1] = Utility.getMinIndex(v[i]);
    }
    int t_start = id[Utility.getMinIndex(id)], t_end = id[Utility.getMaxIndex(id)], r_start = start[0], r_end = end[0];
    for(int i = start[0] + 1; i <= end[0]; i ++) {
      if(data[0].getTimeAt(i) >= data[1].getTimeAt(t_start)) { r_start = i - 1; break; }
    }
    for(int i = end[0] - 1; i >= start[0]; i --) {
      if(data[0].getTimeAt(i) <= data[1].getTimeAt(t_end)) { r_end = i + 1; break; }
    }
    int peak_index = -1, trough_index = -1;
    length = r_end - r_start + 1;
    if(length <= 0) return null;
    double peak = -1000000, trough = 100000, sqsum[] = new double[length];
    for(int i = 0; i < length; i ++) {
      sqsum[i] = Utility.sqsum(data[0].getValues(i + r_start));
      peak_index = peak < sqsum[i] ? i : peak_index;
      trough_index = trough > sqsum[i] ? i : trough_index;
      peak = peak < sqsum[i] ? sqsum[i] : peak;
      trough = trough > sqsum[i] ? sqsum[i] : trough;
    }
    double p[] = {peak - base, trough - base,
      sqsum[0] - base, sqsum[length - 1] - base,
      peak - trough, peak_index - trough_index,
      peak_index * 1.0 / length, trough_index * 1.0 / length,
      Utility.getMean(sqsum), Utility.getStd(sqsum), length};
    return p;
  }

  @Override
  public String getCase(double feature[], int res) {
    return null;
  }
}
