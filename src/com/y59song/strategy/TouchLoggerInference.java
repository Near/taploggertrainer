package com.y59song.strategy;

import com.y59song.utilities.SigWin;

/**
 * Created by frank on 2/25/2014.
 */
public class TouchLoggerInference extends IInferenceStrategy {
  @Override
  public int[] classify(SigWin[] dataWin, int[] start, int[] end) {
    return new int[0];
  }

  @Override
  public double[] getProbability(SigWin[] dataWin, int[] start, int[] end) {
    return new double[0];
  }

  @Override
  public void importModel(String name) {

  }

  @Override
  public void exportModel(String name) {

  }

  @Override
  public void learn(SigWin[] data, int[] start, int[] end, int res) {

  }

  @Override
  public double[] getFeature(SigWin[] data, int[] start, int[] end) {
    return new double[0];
  }
}
