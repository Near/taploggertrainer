package com.y59song.strategy;

import com.y59song.utilities.SigWin;
import com.y59song.utilities.Utility;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

import java.io.*;

/**
 * Created by frank on 2/23/2014.
 */
public class SVMDetection extends IDetectionStrategy {
  public String toOutput = "";
  public svm_model model;
  private final double threshold = 0.5;

  @Override
  public boolean isTap(SigWin[] dataWin, int start[], int end[]) {
    return getProbability(dataWin, start, end) >= threshold;
  }

  public double getProbability(SigWin[] dataWin, int start[], int end[]) {
    double features[] = getFeature(dataWin, start, end), prob[] = new double[model.nr_class];
    svm_node x[] = new svm_node[features.length];
    for(int i = 0; i < features.length; i ++)  {
      x[i] = new svm_node();
      x[i].index = i + 1;
      x[i].value = features[i];
    }
    svm.svm_predict_probability(model, x, prob);
    int index = 0;
    if(model.label[1] == 1) index = 1;
    return prob[index];
  }

  public int[] getTap(SigWin[] dataWin, int end[]) {
    int ret[] = new int[end.length];
    int left = Math.max(end[0] - 13, 0), right = Math.max(end[0] - 5, 0);
    if(left < 0) return null;
    double bestProbability = 0;
    for(int i = left; i <= right; i ++) {
      double temp = getProbability(dataWin, new int[]{i, 0}, end);
      if(bestProbability < temp) {
        bestProbability = temp;
        ret[0] = i;
      }
    }
    if(bestProbability >= threshold) {
      for(int j = end[1]; j >= 0; j --) {
        if(dataWin[1].getTimeAt(j) <= dataWin[0].getTimeAt(ret[0])) {
          ret[1] = j;
          return ret;
        }
      }
    }
    return null;
  }

  @Override
  public void importModel(String name) {
    try {
      model = svm.svm_load_model(name);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void exportModel(String name) {
    Utility.appendToFile(name, toOutput);
  }

  @Override
  public void learn(SigWin[] data, int[] start, int[] end, int res) {
    toOutput = getCase(getFeature(data, start, end), res);
  }

  @Override
  public double[] getFeature(SigWin[] data, int[] start, int[] end) {
    int peak_index = -1, trough_index = -1, length = end[0] - start[0] + 1;
    double peak = -1000000, trough = 100000, sqsum[];
    if(length <= 0) return null;
    sqsum = new double[length];
    for(int i = 0; i < length; i ++) {
      sqsum[i] = Utility.sqsum(data[0].getValues(i + start[0]));
      peak_index = peak < sqsum[i] ? i : peak_index;
      trough_index = trough > sqsum[i] ? i : trough_index;
      peak = peak < sqsum[i] ? sqsum[i] : peak;
      trough = trough > sqsum[i] ? sqsum[i] : trough;
    }
    double p[] = {peak - base, trough - base,
      sqsum[0] - base, sqsum[end[0] - start[0]] - base,
      peak - trough, peak_index - trough_index,
      Utility.getMean(sqsum) - base, Utility.getStd(sqsum), length};
    return p;
  }
}
