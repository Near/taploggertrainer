package com.y59song.strategy;

import android.util.Log;
import com.y59song.utilities.SigWin;
import com.y59song.utilities.Utility;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by frank on 2/23/2014.
 */
public class NormalizedDetection extends IDetectionStrategy {
  private ArrayList<Double>[] datas;
  private double[] features;
  private final int feature_num = 9;
  private final double threshold = 1;

  public NormalizedDetection() {
    features = new double[feature_num * 2];
    datas = new ArrayList[feature_num];
    for(int i = 0; i < feature_num; i ++) datas[i] = new ArrayList<Double>();
  }

  @Override
  public boolean isTap(SigWin[] dataWin, int start[], int end[]) {
    return getProbability(dataWin, start, end) >= threshold;
  }

  public double getProbability(SigWin[] dataWin, int start[], int end[]) {
    double p[] = getFeature(dataWin, start, end), ret = 1.0;
    String output = "";
    if(p == null) return 0;
    for(int i = 0; i < p.length; i ++) {
      double mean = features[i*2], std = features[i*2+1], temp = Utility.getProbability(p[i], mean, std) * 100;
      ret *= temp;
      //if(i != 2) continue;
      output += " item " + i + " : " + (int)(temp * 10000) / 10000.0;
    }
    output += " " + ret;
    if(ret >= threshold) Log.v("Normalization", output);
    return ret;
  }

  public int[] getTap(SigWin[] dataWin, int end[]) {
    int ret[] = new int[end.length];
    int left = Math.max(end[0] - (int)(features[feature_num * 2 - 2] + features[feature_num * 2 - 1]), 0),
      right = Math.max(end[0] - (int)(features[feature_num * 2 - 2] - features[feature_num * 2 - 1]), 0);

    //int left = end[0] - (int)(features[feature_num * 2 - 2]), right = left;
    if(left < 0) return null;
    double bestProbability = 0;
    for(int i = left; i <= right; i ++) {
      double temp = getProbability(dataWin, new int[]{i, 0}, end);
      if(bestProbability < temp) {
        bestProbability = temp;
        ret[0] = i;
      }
    }
    if(bestProbability >= threshold) {
      for(int j = end[1]; j >= 0; j --) {
        if(dataWin[1].getTimeAt(j) <= dataWin[0].getTimeAt(ret[0])) {
          ret[1] = j;
          return ret;
        }
      }
    }
    return null;
  }

  @Override
  public void importModel(String name) {
    File file = new File(name);
    BufferedReader br;
    try {
      br = new BufferedReader(new FileReader(file));
      if(br == null) return;
      for(int i = 0; i < feature_num; i ++) {
        try {
          features[i * 2] = Double.parseDouble(br.readLine());
          features[i * 2 + 1] = Double.parseDouble(br.readLine());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  public void exportModel(String name) {
    File file = new File(name);
    FileWriter fw;
    Log.v(this.getClass().getSimpleName(), "Export file name : " + name);
    try {
      fw = new FileWriter(file);
      if(fw == null) return;
      //file.delete();
      for(int i = 0; i < feature_num; i ++) {
        double data[] = new double[datas[i].size()];
        for(int j = 0; j < data.length; j ++) data[j] = datas[i].get(j);
        double range[] = new double[]{Utility.getMean(data), Utility.getStd(data)};
        System.arraycopy(range, 0, features, i * 2, 2);
      }
      for(int i = 0; i < features.length; i ++) fw.write("" + features[i] + "\n");
      fw.flush();
      fw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void learn(SigWin[] data, int[] start, int[] end, int res) {
    double []p = getFeature(data, start, end);
    if(p == null) return;
		for(int i = 0; i < p.length; i ++) datas[i].add(p[i]);
  }

  @Override
  public double[] getFeature(SigWin[] data, int[] start, int[] end) {
    int peak_index = -1, trough_index = -1, length = end[0] - start[0] + 1;
    double peak = -1000000, trough = 100000, sqsum[];
    if(length <= 0) return null;
    sqsum = new double[length];
    for(int i = 0; i < length; i ++) {
      sqsum[i] = Utility.sqsum(data[0].getValues(i + start[0]));
      peak_index = peak < sqsum[i] ? i : peak_index;
      trough_index = trough > sqsum[i] ? i : trough_index;
      peak = peak < sqsum[i] ? sqsum[i] : peak;
      trough = trough > sqsum[i] ? sqsum[i] : trough;
    }
    double p[] = {peak - base, trough - base,
      sqsum[0] - base, sqsum[end[0] - start[0]] - base,
      peak - trough, peak_index - trough_index,
      Utility.getMean(sqsum) - base, Utility.getStd(sqsum), length};
    return p;
  }

  @Override
  public String getCase(double feature[], int res) {
    return null;
  }
}
