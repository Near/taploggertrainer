package com.y59song.strategy;

import com.y59song.utilities.SigWin;
import com.y59song.utilities.Utility;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

import java.io.*;

/**
 * Created by frank on 2/23/2014.
 */
public class TapLoggerInference extends IInferenceStrategy {
  private String toOutput = "";
  private svm_model model;
  private final int guess_num = 4;
  @Override
  public int[] classify(SigWin[] dataWin, int[] start, int[] end) {
    double prob[] = getProbability(dataWin, start, end);
    int id[] = Utility.getHighest(prob, guess_num);
    for(int i = 0; i < id.length; i ++) id[i] = model.label[id[i]];
    return id;
  }

  public double[] getProbability(SigWin[] dataWin, int[] start, int[] end) {
    double features[] = getFeature(dataWin, start, end), prob[] = new double[model.nr_class];
    svm_node x[] = new svm_node[features.length];
    for(int i = 0; i < features.length; i ++)  {
      x[i] = new svm_node();
      x[i].index = i + 1;
      x[i].value = features[i];
    }
    svm.svm_predict_probability(model, x, prob);
    return prob;
  }

  @Override
  public void importModel(String name) {
    try {
      model = svm.svm_load_model(name);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void exportModel(String name) {
    Utility.appendToFile(name, toOutput);
  }

  @Override
  public void learn(SigWin[] data, int[] start, int[] end, int res) {
    toOutput = getCase(getFeature(data, start, end), res);
  }

  @Override
  public double[] getFeature(SigWin[] data, int[] start, int[] end) {
    int length = end[1] - start[1] + 1, id[][] = new int[2][2];
    long time = data[1].getTimeAt(end[1]) - data[1].getTimeAt(start[1]);
    double v[][] = new double[2][length], pos[][] = new double[2][2];
    for(int i = 0; i < length; i ++) {
      v[0][i] = data[1].getValueAt(i + start[1], 1);
      v[1][i] = data[1].getValueAt(i + start[1], 2);
    }
    double[] res = new double[10];
    for(int i = 0; i < 2; i ++) {
      id[0][i] = Utility.getMaxIndex(v[i]);
      id[1][i] = Utility.getMinIndex(v[i]);
      pos[0][i] = 1.0 * (data[1].getTimeAt(id[0][i] + start[1]) - data[1].getTimeAt(start[1])) / time;
      pos[1][i] = 1.0 * (data[1].getTimeAt(id[1][i] + start[1]) - data[1].getTimeAt(start[1])) / time;
      int x = pos[0][i] > pos[1][i] ? 1 : 0;
      res[i * 2 + x] = v[i][id[0][i]] - Utility.getMean(v[i]);
      res[i * 2 + 1 - x] = v[i][id[1][i]] - Utility.getMean(v[i]);
      res[i * 2 + 4 + x] = pos[0][i];
      res[i * 2 + 5 - x] = pos[1][i];
    }
    res[8] = v[0][length - 1] - v[0][0];
    res[9] = v[1][length - 1] - v[1][0];
    return res;
  }
}
