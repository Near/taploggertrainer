package com.y59song.defense;

import com.y59song.utilities.Reading;
import com.y59song.utilities.SigWin;

/**
 * Created by y59song on 28/02/14.
 */
public class LowerRate implements IDefense {
  private int lower, countA = 0, countO = 0;
  public LowerRate(int l) {
    lower = l;
  }
  @Override
  public void handle_accelerometer(SigWin data, Reading r) {
    if(countA % lower == 0) data.addReading(r);
    countA = (countA + 1) % lower;
  }

  @Override
  public void handle_orientation(SigWin data, Reading r) {
    if(countO % lower == 0) data.addReading(r);
    countO = (countO + 1) % lower;
  }
}
