package com.y59song.defense;

import com.y59song.utilities.Reading;
import com.y59song.utilities.SigWin;

/**
 * Created by y59song on 28/02/14.
 */
public class Normal implements IDefense {

  @Override
  public void handle_accelerometer(SigWin data, Reading r) {
    data.addReading(r);
  }

  @Override
  public void handle_orientation(SigWin data, Reading r) {
    data.addReading(r);
  }
}
