package com.y59song.defense;

import com.y59song.utilities.Reading;
import com.y59song.utilities.SigWin;
import com.y59song.utilities.Utility;

import java.util.Random;

/**
 * Created by y59song on 28/02/14.
 */
public class AddNoise implements IDefense {
  private double probability = 0;
  private Random rn = new Random();
  public AddNoise(double p) {
    probability = p;
  }
  @Override
  public void handle_accelerometer(SigWin data, Reading r) {
    //if(Utility.rn.nextDouble() < probability) r = new Reading(Utility.getRandomArray(r.getValues().length), r.getTime());
    data.addReading(r);
  }

  @Override
  public void handle_orientation(SigWin data, Reading r) {
    if(Utility.rn.nextDouble() < probability) r = new Reading(Utility.getRandomArray(r.getValues().length), r.getTime());
    data.addReading(r);
  }
}
