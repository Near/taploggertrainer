package com.y59song.defense;

import com.y59song.utilities.*;

/**
 * Created by y59song on 28/02/14.
 */
public interface IDefense {
  public void handle_accelerometer(SigWin data, Reading r);
  public void handle_orientation(SigWin data, Reading r);
}
