package com.y59song.utilities;

import java.util.ArrayList;

/**
 * Created by frank on 2/23/2014.
 */
public class SigWin {
  private ArrayList<Reading> datalist;
  private int preWin = 0, postWin = 0;

  public SigWin() {
    datalist = new ArrayList<Reading>();
  }

  public void setWinSize(int pre, int post) {
    preWin = pre;
    postWin = post;
  }

  public void addReading(Reading elem) {
    datalist.add(elem);
  }

  public int getStartIndex() {
    return Math.max(0, datalist.size() - preWin - 1);
  }

  public int getEndIndex() {
    // TODO
    return datalist.size() - 1;
  }

  public double getValueAt(int pos, int index) {
    return datalist.get(pos).getValue(index);
  }

  public double[] getValues(int pos) {
    return datalist.get(pos).getValues();
  }

  public long getTimeAt(int pos) {
    return datalist.get(pos).getTime();
  }
}
