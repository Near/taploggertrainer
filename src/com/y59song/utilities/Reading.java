package com.y59song.utilities;

/**
 * Created by frank on 2/23/2014.
 */
public class Reading {
  private double values[];
  private long time;
  public Reading(float v[], long t) {
    values = new double[v.length];
    for(int i = 0; i < v.length; i ++) values[i] = v[i];
    time = t;
  }

  public double getValue(int index) {
    return values[index];
  }

  public double[] getValues() {
    return values;
  }

  public long getTime() {
    return time;
  }
}
