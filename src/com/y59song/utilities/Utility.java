package com.y59song.utilities;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by frank on 2/23/2014.
 */
public class Utility {
  public static Random rn = new Random();

  public static double sqsum(double[] v) {
    double ans = 0;
    for(double e : v) ans += e * e;
    return ans;
  }

  public static double getMean(double[] v) {
    double ans = getSum(v);
    if(v.length > 0) ans /= v.length;
    return ans;
  }

  public static double getStd(double[] v) {
    double mean = getMean(v), ans = 0;
    for(double e : v) ans += (e - mean) * (e - mean);
    return Math.sqrt(ans / (v.length - 1));
  }

  public static double getSum(double[] v) {
    double ret = 0;
    for(double e : v) ret += e;
    return ret;
  }

  public static double[] getRange(double[] v) {
    double r[] = new double[2];
    Arrays.sort(v);
    double q1 = v[v.length / 4], q3 = v[v.length * 3/ 4];
    r[0] = q1 - (q3 - q1) * 0.7;
    r[1] = q3 + (q3 - q1) * 0.7;
    return r;
  }

  public static int[] getHighest(double[] prob, int num) {
    HashMap<Double, Integer> map = new HashMap<Double, Integer>();
    int ret[] = new int[num];
    for(int i = 0; i < prob.length; i ++) map.put(prob[i], i);
    Arrays.sort(prob);
    for(int i = 0; i < num; i ++) {
      ret[i] = map.get(prob[prob.length - i - 1]);
    }
    return ret;
  }

  public static double getHighestSum(double[] prob, int num) {
    int id[] = getHighest(prob, num);
    double ret = 0;
    for(int i : id) ret += prob[i];
    return ret;
  }

  public static float[] getRandomArray(int length) {
    float ret[] = new float[length];
    for(int i = 0; i < length; i ++) {
      ret[i] = rn.nextFloat();
    }
    return ret;
  }

  // direction : true inc, false dec
  public static int getMaxIndex(double[] v) {
    int ret = 0;
    for(int i = 1; i < v.length; i ++) ret = v[i] > v[ret] ? i : ret;
    return ret;
  }

  public static int getMinIndex(double[] v) {
    int ret = 0;
    for(int i = 1; i < v.length; i ++) ret = v[i] < v[ret] ? i : ret;
    return ret;
  }

  public static int getMaxIndex(int[] v) {
    int ret = 0;
    for(int i = 1; i < v.length; i ++) ret = v[i] > v[ret] ? i : ret;
    return ret;
  }

  public static int getMinIndex(int[] v) {
    int ret = 0;
    for(int i = 1; i < v.length; i ++) ret = v[i] < v[ret] ? i : ret;
    return ret;
  }

  public static double getProbability(double v, double mean, double std) {
    return Math.exp(- Math.pow(v-mean, 2.0) / 2 / Math.pow(std, 2.0)) / Math.sqrt(2 * 3.1415926) / std;
  }

  public static void deleteFile(String file) {
    File f = new File(file);
    f.delete();
  }

  public static void appendToFile(String file, String content) {
    PrintWriter out = null;
    try {
      out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
    } catch (IOException e) {
      e.printStackTrace();
    }
    out.println(content);
    out.close();
  }

  /*
  public static svm_problem read_problem(String file) throws IOException {
    BufferedReader fp = new BufferedReader(new FileReader(file));
    Vector<Double> vy = new Vector<Double>();
    Vector<svm_node[]> vx = new Vector<svm_node[]>();
    int max_index = 0;

    while(true)
    {
      String line = fp.readLine();
      if(line == null) break;

      StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");

      vy.addElement(atof(st.nextToken()));
      int m = st.countTokens()/2;
      svm_node[] x = new svm_node[m];
      for(int j=0;j<m;j++)
      {
        x[j] = new svm_node();
        x[j].index = atoi(st.nextToken());
        x[j].value = atof(st.nextToken());
      }
      if(m>0) max_index = Math.max(max_index, x[m-1].index);
      vx.addElement(x);
    }

    svm_problem prob = new svm_problem();
    prob.l = vy.size();
    prob.x = new svm_node[prob.l][];
    for(int i=0;i<prob.l;i++)
      prob.x[i] = vx.elementAt(i);
    prob.y = new double[prob.l];
    for(int i=0;i<prob.l;i++)
      prob.y[i] = vy.elementAt(i);

    if(param.gamma == 0 && max_index > 0)
      param.gamma = 1.0/max_index;

    if(param.kernel_type == svm_parameter.PRECOMPUTED)
      for(int i=0;i<prob.l;i++)
      {
        if (prob.x[i][0].index != 0)
        {
          System.err.print("Wrong kernel matrix: first column must be 0:sample_serial_number\n");
          System.exit(1);
        }
        if ((int)prob.x[i][0].value <= 0 || (int)prob.x[i][0].value > max_index)
        {
          System.err.print("Wrong input format: sample_serial_number out of range\n");
          System.exit(1);
        }
      }

    fp.close();
  }
  */
}
