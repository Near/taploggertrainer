package com.y59song.logger;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.y59song.defense.*;
import com.y59song.strategy.*;
import com.y59song.utilities.*;
/**
 * Created by frank on 2/23/2014.
 */
public class Logger extends LinearLayout implements SensorEventListener {
  private final int ACCELEROMETER = 0, ORIENTATION = 1;
  private IDetectionStrategy detection = new TapLoggerDetection();
  private IInferenceStrategy inference = new TapLoggerInference();
  private IDefense defense = new LowerRate(2);
  private final int sensor_num = 2, guess_num = 4, max_delay = 30;
  private SigWin[] data; // 0 for accelerator, 1 for orientation
  private int[] startIndex, endIndex;
  private int tapcount = 0, count[] = new int[guess_num], detected = 0, delay = max_delay, falsepositive = 0;
  private boolean touched = false, realtouched = false;
  private ToyKeyboard keyboard;
  private TextView infer, realtap, detectedtap;

  public Logger(Context context, AttributeSet attrs) {
    super(context, attrs);
    startIndex = new int[sensor_num];
    endIndex = new int[sensor_num];
    data = new SigWin[sensor_num];
    for(int i = 0; i < sensor_num; i ++) data[i] = new SigWin();
    for(int i = 0; i < guess_num; i ++) count[i] = 0;
    SensorManager mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
    Sensor mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    Sensor mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    keyboard = new ToyKeyboard(context, attrs);
    keyboard.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));
    detection.importModel(Environment.getExternalStorageDirectory().getPath() + "/detection_data");
    inference.importModel(Environment.getExternalStorageDirectory().getPath() + "/output");
    this.addView(keyboard);
    mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_FASTEST);
  }

  public void setModel(TextView m1, TextView m2, TextView m3) {
    infer = m1;
    realtap = m2;
    detectedtap = m3;
  }

  public boolean dispatchTouchEvent(MotionEvent event) {
    switch(event.getAction()) {
      case MotionEvent.ACTION_DOWN :
        realtouched = true;
        for(int i = 0; i < sensor_num; i ++) startIndex[i] = data[i].getStartIndex();
        break;
      case MotionEvent.ACTION_UP :
        realtouched = false;
        tapcount += 1;
        for(int i = 0; i < sensor_num; i ++) endIndex[i] = data[i].getEndIndex();
        realtap.setText("real : " + tapcount + " start : " + startIndex[0] + " end : " + endIndex[0] + " p : " +
          detection.getProbability(data, startIndex, endIndex));

        inference.learn(data, startIndex, endIndex, keyboard.getLast());
        inference.exportModel(Environment.getExternalStorageDirectory().getPath() + "/inference_data");
        infer.setText(getInf(inference.classify(data, startIndex, endIndex), keyboard.getLast()));

        break;
      default : break;
    }
    return super.dispatchTouchEvent(event);
  }

  @Override
  public void onSensorChanged(SensorEvent event) {
    switch(event.sensor.getType()) {
      case Sensor.TYPE_ACCELEROMETER :
        defense.handle_accelerometer(data[ACCELEROMETER], new Reading(event.values, event.timestamp));
        //data[ACCELEROMETER].addReading(new Reading(event.values, event.timestamp));
/*
        if(touched) {
          delay --;
          if(delay == 0) { touched = false; delay = max_delay; }
          else break;
        }
        int end[] = new int[]{data[ACCELEROMETER].getEndIndex(), data[ORIENTATION].getEndIndex()};
        int start[] = detection.getTap(data, end);
        if(start != null) {
          double[] prob = inference.getProbability(data, start, end);
          //if(Utility.getHighestSum(prob, 1) < 0.2 || Utility.getHighestSum(prob, 4) < 0.5) break;
          touched = true;
          if(!realtouched) { falsepositive += 1; break; }
          detected += 1;
          detectedtap.setText("detected : " + detected + " , falsepositive : " + falsepositive +
            " start : " + start[0] + " end : " + end[0]);
          inference.learn(data, start, end, keyboard.getLast());
          inference.exportModel(Environment.getExternalStorageDirectory().getPath() + "/inference_data");
          infer.setText(getInf(inference.classify(data, start, end), keyboard.getLast()));
        }
*/
        break;
      case Sensor.TYPE_ORIENTATION :
        defense.handle_accelerometer(data[ORIENTATION], new Reading(event.values, event.timestamp));
        //data[ORIENTATION].addReading(new Reading(event.values, event.timestamp));
        break;
      default : break;
    }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) { }

  private String getInf(int ret[], int ans) {
    boolean hit = false;
    String inf = "";
    for(int i = 0; i < ret.length; i ++) {
      inf += "" + ret[i] + " ";
      hit = hit || (ret[i] == ans);
      if(hit) count[i] ++;
    }
    if(tapcount == 0) return inf;
    inf += "inference : " + (count[3] * 100 / tapcount) + "%;";
    inf += "\n";
    for(int i = 1; i <= ret.length; i ++) {
      inf += "top-" + i + " inference : " + (count[i-1] * 100 / tapcount) + "%; ";
    }
    return inf;
  }
}
