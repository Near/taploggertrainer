package com.y59song.logger;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import com.example.trainer.R;

/**
 * Created by y59song on 24/02/14.
 */
public class LoggerHomeScreen extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Logger logger;
    TextView realtap, detectedtap, inference;
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_logger);
    realtap = (TextView)findViewById(R.id.realtap);
    detectedtap = (TextView)findViewById(R.id.detectedtap);
    inference = (TextView)findViewById(R.id.inference);
    logger = (Logger)findViewById(R.id.logger);
    logger.setModel(inference, realtap, detectedtap);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.logger, menu);
    return true;
  }
}
