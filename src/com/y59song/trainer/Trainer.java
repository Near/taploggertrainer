package com.y59song.trainer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.y59song.defense.*;
import com.y59song.strategy.*;
import com.y59song.utilities.*;

/**
 * Created by frank on 2/23/2014.
 */
public class Trainer extends LinearLayout implements SensorEventListener {
  private final int ACCELEROMETER = 0, ORIENTATION = 1;
  private IDetectionStrategy detection = new TapLoggerDetection();
  private IInferenceStrategy inference = new TapLoggerInference();
  private IDefense defense = new LowerRate(2);
  private final int sensor_num = 2;
  private SigWin[] data; // 0 for accelerator, 1 for orientation
  private int[] startIndex, endIndex;
  private boolean inTouch = false;
  private ToyKeyboard keyboard;
  private TextView model;

  public Trainer(Context context, AttributeSet attrs) {
    super(context, attrs);
    startIndex = new int[sensor_num];
    endIndex = new int[sensor_num];
    data = new SigWin[sensor_num];
    for(int i = 0; i < sensor_num; i ++) data[i] = new SigWin();
    SensorManager mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
    Sensor mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    Sensor mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    keyboard = new ToyKeyboard(context, attrs);
    keyboard.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));
    this.addView(keyboard);
    mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_FASTEST);
  }

  public void setModel(TextView m) {
    model = m;
  }

  public boolean dispatchTouchEvent(MotionEvent event) {
    switch(event.getAction()) {
      case MotionEvent.ACTION_DOWN :
        inTouch = true;
        for(int i = 0; i < sensor_num; i ++) startIndex[i] = data[i].getStartIndex();
        break;
      case MotionEvent.ACTION_UP :
        inTouch = false;
        for(int i = 0; i < sensor_num; i ++) endIndex[i] = data[i].getEndIndex();
        detection.learn(data, startIndex, endIndex, 1);
        detection.exportModel(Environment.getExternalStorageDirectory().getPath() + "/detection_data");
        inference.learn(data, startIndex, endIndex, keyboard.getLast());
        inference.exportModel(Environment.getExternalStorageDirectory().getPath() + "/inference_data");
        model.setText("Last : " + keyboard.getLast() + " " + ((int)(Math.random() * 1000)) % 12);
        break;
      default : break;
    }
    return super.dispatchTouchEvent(event);
  }

  @Override
  public void onSensorChanged(SensorEvent event) {
    switch(event.sensor.getType()) {
      case Sensor.TYPE_ACCELEROMETER :
        defense.handle_accelerometer(data[ACCELEROMETER], new Reading(event.values, event.timestamp));
        //data[ACCELEROMETER].addReading(new Reading(event.values, event.timestamp));
        // just for false case
        // TODO make the false cases more general
        /*
        if(data[ACCELEROMETER].getEndIndex() % 2 == 0) break;
        else if(!inTouch) detection.learn(data, new int[]{Math.max(data[ACCELEROMETER].getEndIndex() - 9, 0), 0}, new int[]{data[ACCELEROMETER].getEndIndex(), 0}, 0);
        else detection.learn(data, startIndex, new int[]{data[ACCELEROMETER].getEndIndex(), 0}, 0);
        detection.exportModel(Environment.getExternalStorageDirectory().getPath() + "/detection_data");
        */
        //
        break;
      case Sensor.TYPE_ORIENTATION :
        defense.handle_accelerometer(data[ORIENTATION], new Reading(event.values, event.timestamp));
        //data[ORIENTATION].addReading(new Reading(event.values, event.timestamp));
        break;
      default : break;
    }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) { }
}
