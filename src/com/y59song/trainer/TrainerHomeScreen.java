package com.y59song.trainer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import com.example.trainer.R;

public class TrainerHomeScreen extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Trainer trainer;
    TextView feature;
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_trainer);
    feature = (TextView)findViewById(R.id.feature);
    trainer = (Trainer)findViewById(R.id.loggerTrainer);
    trainer.setModel(feature);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.trainer, menu);
    return true;
  }

}
