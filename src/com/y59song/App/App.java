package com.y59song.App;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import com.example.trainer.R;
import com.y59song.logger.LoggerHomeScreen;
import com.y59song.trainer.TrainerHomeScreen;
import com.y59song.utilities.Utility;

/**
 * Created by frank on 2/24/2014.
 */
public class App extends Activity {
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_app);
    Utility.deleteFile(Environment.getExternalStorageDirectory().getPath() + "/detection_data");
    Utility.deleteFile(Environment.getExternalStorageDirectory().getPath() + "/inference_data");
  }

  public void click(View v) {
    Class togo = v.getId() == R.id.Train ? TrainerHomeScreen.class : LoggerHomeScreen.class;
    Intent intent = new Intent(this, togo);
    startActivity(intent);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.app, menu);
    return true;
  }
}
